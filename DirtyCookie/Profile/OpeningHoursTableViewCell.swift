//
//  OpeningHoursTableViewCell.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 13/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class OpeningHoursTableViewCell: UITableViewCell {
    
    //MARK:- IBOutlets
    let timeLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = UIColor.dcPink
        return label
    }()
    
    //MARK:- Initialisers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupTimeLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- UI setup
    fileprivate func setupTimeLabel() {
        addSubview(timeLabel)
        timeLabel.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
        timeLabel.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
    }
}
