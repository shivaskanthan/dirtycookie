//
//  ProfileViewController.swift
//  DirtyCookie
//
//  Created by Meitar Basson on 02/03/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit
import FirebaseAuth
import MessageUI
import SafariServices
import MapKit

class ProfileViewController: UIViewController {
    
    // MARK:- Variables and constants
    var currentUser: User!
    let reuseIdentifier = "ProfileCell"
    
    //MARK:- IBOutlets
    let profileTable: UITableView = {
        let tv = UITableView()
        tv.rowHeight = 60
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    let sectionHeaderTitleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let footerView: UIView = {
        let view = UIView()
        view.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 87)
        return view
    }()
    
    let logoutButton: UIButton = {
        let button = UIButton()
        button.layer.borderColor = UIColor.dcPink.cgColor
        button.layer.borderWidth = 2
        button.setTitle("Log out", for: .normal)
        button.setTitleColor(UIColor.dcPink, for: .normal)
        button.layer.cornerRadius = 9
        button.addTarget(self, action: #selector(logoutButtonPressed), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        // Setup navigation item title
        self.navigationItem.title = "Profile"
        // Check if user is logged in
        if AuthService.shared.isUserLoggedIn() == true {
            retrieveUserDetails()
            setupUI()
        } else {
            // Sends user to sign in or sign up screen if they are not authorised
            debugPrint("User is not signed in")
            handleUnauthorisedUser()
        }
    }
    
    //MARK:- UI Setup
    fileprivate func setupUI() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editProfile))
        setupTableView()
    }
    
    fileprivate func setupTableView() {
        profileTable.delegate = self
        profileTable.dataSource = self
        profileTable.register(ProfileTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        setupFooterView()
        view.addSubview(profileTable)
        profileTable.frame = view.frame
    }
    
    fileprivate func setupFooterView() {
        setupLogoutButton()
        profileTable.tableFooterView = footerView
    }
    
    fileprivate func setupLogoutButton() {
        footerView.addSubview(logoutButton)
        logoutButton.centerYAnchor.constraint(equalTo: footerView.centerYAnchor).isActive = true
        logoutButton.leftAnchor.constraint(equalTo: footerView.leftAnchor, constant: 20).isActive = true
        logoutButton.rightAnchor.constraint(equalTo: footerView.rightAnchor, constant: -20).isActive = true
    }
    
    //MARK:- IBAction
    @objc func editProfile() {
        debugPrint("Edit Profile")
    }
    
    @objc func logoutButtonPressed() {
        // Logout alert action
        let logoutAction = UIAlertAction(title: "OK", style: .destructive) { (action) in
            self.logoutUser()
        }
        // Logout alert
        let logoutAlert = Alert.showActionSheet(title: "Are you sure?", message: nil, handlers: [logoutAction])
        present(logoutAlert, animated: true, completion: nil)
    }
    
    //MARK:- Functions
    
    //MARK: Our Shop
    // Preseent a alert action sheet for the user to choose between Apple Maps and Google Maps to navigate to the Dirty Cookie store
    fileprivate func chooseMapApp() {
        let appleMapsActtion = UIAlertAction(title: "Open in Apple Maps", style: .default) { (action) in
            self.openAppleMaps()
        }
        let googleMapsAction = UIAlertAction(title: "Open in Google Maps", style: .default) { (action) in
            self.openGoogleMaps()
        }
        let openMapsAlert = Alert.showActionSheet(title: "Choose an app to view our location", message: nil, handlers: [appleMapsActtion, googleMapsAction])
        present(openMapsAlert, animated: true, completion: nil)
    }
    
    // Open Apple Maps
    fileprivate func openAppleMaps() {
        // Create a coordinate with DC's latitude and longitude
        guard let latitude = CLLocationDegrees(exactly: k_LOCATION_LATITUDE) else { return }
        guard let longitude = CLLocationDegrees(exactly: k_LOCATION_LONGITUDE) else { return }
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        // Create a mapItem with the coordinate and company name
        let placemark = MKPlacemark(coordinate: coordinate)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = kCOMPANY_DETAILS_NAME
        // Use the mapItem to open Apple Maps and display DC's location
        mapItem.openInMaps(launchOptions: nil)
    }
    
    // Open Google Maps
    fileprivate func openGoogleMaps() {
        // Create Google Maps app and web URL
        guard let googleMapsAppURL = URL(string: kAPPURL_GOOGLE_MAPS) else { return debugPrint("Could not retrieve Google Maps app URL")}
        guard let googleMapsWebURL = createURLFor(scheme: kURL_SCHEME, host: kURL_HOST_GOOGLE_MAPS, path: kURL_PATH_GOOGLE_MAPS) else { return debugPrint("Google Maps url could not be created from url components")}
        // Open DC location in Google Maps app if available else open in Safari
        openExternalApp(app: googleMapsAppURL, web: googleMapsWebURL)
    }
    
    //MARK: Opening Hours
    fileprivate func goToOpeningHours() {
        let openingHoursVC = OpeningHoursViewController()
        navigationController?.pushViewController(openingHoursVC, animated: true)
    }
    
    //MARK: Follow us on Instagram
    fileprivate func goToInstagram() {
        // Create Instagram app and web URL
        guard let instagramAppURL = URL(string: kAPPURL_INSTAGRAM) else { return debugPrint("Could not retrieve Instagram app URL")}
        guard let instagramWebURL = createURLFor(scheme: kURL_SCHEME, host: kURL_HOST_INSTAGRAM, path: kURL_PATH_INSTAGRAM) else { return debugPrint("Instagram url could not be created from url components")}
        // Open DC profile in Instagram app if available else open in Safari
        openExternalApp(app: instagramAppURL, web: instagramWebURL)
    }
    
    
    //MARK: Like us on Facebook
    fileprivate func goToFacebook() {
        // Create Facebook app and web URL
        guard let facebookAppURL = URL(string: kAPPURL_FACEBOOK) else { return debugPrint("Could not retrieve Facebook app URL")}
        guard let facebookWebURL = createURLFor(scheme: kURL_SCHEME, host: kURL_HOST_FACEBOOK, path: kURL_PATH_FACEBOOK) else { return debugPrint("Facebook url could not be created from url components")}
        // Open DC profile in Facebook app if available else open in Safari
        openExternalApp(app: facebookAppURL, web: facebookWebURL)
    }
    
    //MARK: Feedback
    fileprivate func showMailComposer() {
        // Check if the user can send mail
        guard MFMailComposeViewController.canSendMail() else {
            // Show Alert informing the user
            return
        }
        let composer = MFMailComposeViewController()
        composer.mailComposeDelegate = self
        composer.setToRecipients([kCOMPANY_DETAILS_EMAIL])
        composer.setSubject("App Feedback")
        present(composer, animated: true)
    }
    //MARK: Privacy Policy
    fileprivate func showPrivacyPolicyWebsite() {
        guard let privacyPolicyURL = createURLFor(scheme: kURL_SCHEME, host: kURL_HOST_THEDIRTYCOOKIE, path: kURL_PATH_PRIVACYPOLICY) else { return debugPrint("Privacy policy url could not be created from url components")}
        let safariVC = SFSafariViewController(url: privacyPolicyURL)
        safariVC.preferredControlTintColor = UIColor.dcPink
        present(safariVC, animated: true, completion: nil)
    }
    
    //MARK: Log out
    fileprivate func logoutUser() {
        AuthService.shared.signOutUser { (isSuccess) in
            if isSuccess == true {
                DispatchQueue.main.async {
                    self.goToWelcomeScreen()
                }
            } else {
                // handle case where user can't sign out
            }
        }
    }
    
    //MARK:- Helper functions
    
    // Request user to either sign in or sign up to view ProfileVC
    fileprivate func handleUnauthorisedUser() {
        // We will use this to handle users who are not signed in
        // Plan will be to give them the options to sign up or sign in
        // When they tap, app will segue to the respective screen
    }
    
    // Retrieves the logged in user's fullname and email address
    fileprivate func retrieveUserDetails() {
        // Retrieves logged in user UID
        guard let uid = Auth.auth().currentUser?.uid else { return debugPrint("Could not retrieve user UID")}
        // Return logged in user's fullname and email address in a User model
        DataService.shared.retrieveUserDetailsFor(uid: uid, handler: { (user, error) in
            if let error = error {
                debugPrint("Error retrieving user: ", error.localizedDescription)
                return
            }
            guard let user = user else { return debugPrint("Failed to retrieve user details")}
            self.currentUser = user
            self.profileTable.reloadData()
        })
    }
    
    // Presents the welcome screen
    fileprivate func goToWelcomeScreen() {
        navigationController?.popViewController(animated: false)
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeVC = mainStoryboard.instantiateInitialViewController() else {
            debugPrint("Failed to retrieve WelcomeViewController")
            return
        }
        self.present(welcomeVC, animated: false, completion: nil)
    }
    
    // Creates a URL for a given scheme, host and path
    fileprivate func createURLFor(scheme: String, host: String, path: String) -> URL? {
        var urlComponents = URLComponents()
        urlComponents.scheme = scheme
        urlComponents.host = host
        urlComponents.path = path
        guard let privacyPolicyURL = urlComponents.url else { return nil }
        return privacyPolicyURL
    }
    
    // Opens an app with
    fileprivate func openExternalApp(app appURL: URL, web webURL: URL) {
        let application = UIApplication.shared
        // Open DC profile in app if available else open link in Safari
        if application.canOpenURL(appURL) {
            application.open(appURL)
        } else {
            application.open(webURL)
        }
    }
}

//MARK:- TableView delegate and datasource
extension ProfileViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return ProfileSection.allCases.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let section = ProfileSection(rawValue: section) else {return 0}
        switch section {
        case .Account: return AccountOptions.allCases.count
        case .Notification: return NotificationOptions.allCases.count
        case .TheDirtyCookie: return TheDirtyCookieOptions.allCases.count
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let sectionHeaderView = UIView()
        let titleLabel = UILabel()
        titleLabel.font = UIFont.systemFont(ofSize: 13)
        titleLabel.textColor = .gray
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        titleLabel.text = ProfileSection(rawValue: section)?.description
        sectionHeaderView.addSubview(titleLabel)
        titleLabel.centerYAnchor.constraint(equalTo: sectionHeaderView.centerYAnchor).isActive = true
        titleLabel.leftAnchor.constraint(equalTo: sectionHeaderView.leftAnchor, constant: 16).isActive = true
        return sectionHeaderView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: reuseIdentifier, for: indexPath) as! ProfileTableViewCell
        guard let section = ProfileSection(rawValue: indexPath.section) else {return ProfileTableViewCell()}
        switch section {
        case .Account:
            guard let account = AccountOptions(rawValue: indexPath.row) else { return ProfileTableViewCell()}
            cell.sectionType = account
            switch account {
            case .fullName: cell.cellTextfield.text = currentUser?.fullName ?? ""
            case .email: cell.cellTextfield.text = currentUser?.email ?? ""
            case .deleteAccount: cell.textLabel?.textColor = UIColor.dcPink
            default: break
            }
        case .Notification:
            let notification = NotificationOptions(rawValue: indexPath.row)
            cell.sectionType = notification
        case .TheDirtyCookie:
            let weTheDirtyCookie = TheDirtyCookieOptions(rawValue: indexPath.row)
            cell.sectionType = weTheDirtyCookie
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // Remove gray background when cell is tapped
        tableView.deselectRow(at: indexPath, animated: false)
        
        guard let section = ProfileSection(rawValue: indexPath.section) else {return}
        switch section {
        case .Account:
            debugPrint(AccountOptions(rawValue: indexPath.row)!.description)
        case .Notification:
            debugPrint(NotificationOptions(rawValue: indexPath.row)!.description)
        case .TheDirtyCookie:
            if indexPath.row ==  TheDirtyCookieOptions.ourShop.rawValue {
                chooseMapApp()
            }
            if indexPath.row == TheDirtyCookieOptions.ourOpeningHours.rawValue {
                goToOpeningHours()
            }
            if indexPath.row == TheDirtyCookieOptions.followOnInstagram.rawValue {
                goToInstagram()
            }
            if indexPath.row == TheDirtyCookieOptions.likeOnFacebook.rawValue {
                goToFacebook()
            }
            if indexPath.row ==  TheDirtyCookieOptions.feedback.rawValue {
                showMailComposer()
            }
            if indexPath.row ==  TheDirtyCookieOptions.privacyPolicy.rawValue {
                showPrivacyPolicyWebsite()
            }
        }
    }
}

//MARK:- MFMailComposeView delegate
extension ProfileViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        if let _ = error {
            // Show error alert
            controller.dismiss(animated: true)
        }
        
        switch result {
        case .cancelled:
            debugPrint("Cancelled")
        case .failed:
            debugPrint("Failed")
        case .saved:
            debugPrint("Saved")
        case .sent:
            debugPrint("Email Sent")
        }
        controller.dismiss(animated: true)
    }
}
