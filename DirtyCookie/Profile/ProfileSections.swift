//
//  ProfileSections.swift
//  DirtyCookie
//
//  Created by Meitar Basson on 09/03/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import Foundation

//MARK:- Sub section requirements
//Each section will contain options, each options must conform to the requirements below to ensure UI is set up accordingly
protocol SectionType: CustomStringConvertible {
    var containsSwitch: Bool {get}
    var containsTextField: Bool {get}
    var allowsSegueToNextScreen: Bool {get}
}

//MARK:- Profile sections
enum ProfileSection: Int, CaseIterable, CustomStringConvertible {
    case Account
    case Notification
    case TheDirtyCookie
    
    var description: String {
        switch self {
        case .Account: return "ACCOUNT"
        case .Notification: return "NOTIFICATION"
        case .TheDirtyCookie: return kCOMPANY_DETAILS_NAME.uppercased()
        }
    }
}

//MARK:- Account section
enum AccountOptions: Int, CaseIterable, SectionType {
    case fullName
    case email
    case updatePassword
    case paymentMethod
    case deleteAccount
    
    var description: String {
        switch self {
        case .fullName: return "Fullname"
        case .email: return "Email"
        case .updatePassword: return "Update Password"
        case .paymentMethod: return "Payment Method"
        case .deleteAccount: return "Delete Account"
        }
    }
    
    var containsSwitch: Bool {
        switch self {
        case .fullName: return false
        case .email: return false
        case .updatePassword: return false
        case .paymentMethod: return false
        case .deleteAccount: return false
        }
    }
    
    var containsTextField: Bool {
        switch self {
        case .fullName: return true
        case .email: return true
        case .updatePassword: return false
        case .paymentMethod: return false
        case .deleteAccount: return false
        }
    }
    
    var allowsSegueToNextScreen: Bool {
        switch self {
        case .fullName: return false
        case .email: return false
        case .updatePassword: return true
        case .paymentMethod: return true
        case .deleteAccount: return false
        }
    }
}

//MARK:- Notification section
enum NotificationOptions: Int, CaseIterable, SectionType {
    case appNotification
    
    var description: String {
        switch self {
        case .appNotification: return "App Notifications"
        }
    }
    
    var containsSwitch: Bool {
        switch self {
        case .appNotification: return true
        }
    }
    
    var containsTextField: Bool {
        switch self {
        case .appNotification: return false
        }
    }
    
    var allowsSegueToNextScreen: Bool {
        switch self {
        case .appNotification: return false
        }
    }
}

//MARK:- We The Dirty Cookie section
enum TheDirtyCookieOptions: Int, CaseIterable, SectionType {
    case ourShop
    case ourOpeningHours
    case followOnInstagram
    case likeOnFacebook
    case feedback
    case privacyPolicy
    
    var description: String {
        switch self {
        case .ourShop: return "Our Shop"
        case .ourOpeningHours: return "Our Opening Hours"
        case .followOnInstagram:  return "Follow us on Instagram"
        case .likeOnFacebook: return "Like us on Facebook"
        case .feedback: return "Feedback"
        case .privacyPolicy: return "Privacy Policy"
        }
    }
    
    var containsSwitch: Bool {
        switch self {
        case .ourShop: return false
        case .ourOpeningHours: return false
        case .followOnInstagram: return false
        case .likeOnFacebook: return false
        case .feedback: return false
        case .privacyPolicy: return false
        }
    }
    
    var containsTextField: Bool {
        switch self {
        case .ourShop: return false
        case .ourOpeningHours: return false
        case .followOnInstagram: return false
        case .likeOnFacebook: return false
        case .feedback: return false
        case .privacyPolicy: return false
        }
    }
    
    var allowsSegueToNextScreen: Bool {
        switch self {
        case .ourShop: return false
        case .ourOpeningHours: return true
        case .followOnInstagram: return false
        case .likeOnFacebook: return false
        case .feedback: return true
        case .privacyPolicy: return true
        }
    }
    
}
