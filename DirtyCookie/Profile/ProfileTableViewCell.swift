//
//  ProfileTableViewCell.swift
//  DirtyCookie
//
//  Created by Meitar Basson on 09/03/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {
    
    //MARK:- Variables and constants
    var sectionType: SectionType? {
        didSet {
            guard let sectionType = sectionType else {return}
            textLabel?.text = sectionType.description
            if sectionType.containsSwitch == true {
                setupSwitchControl()
            }
            cellTextfield.isHidden = !sectionType.containsTextField
            if sectionType.allowsSegueToNextScreen == true {
                accessoryType = .disclosureIndicator
            }
        }
    }
    
    lazy var cellTextfield: UITextField = {
        let textfield = UITextField()
        textfield.textColor = UIColor.dcPink
        textfield.translatesAutoresizingMaskIntoConstraints = false
        textfield.isHidden = true
        textfield.borderStyle = UITextField.BorderStyle.none
        textfield.textAlignment = NSTextAlignment.right
        textfield.isUserInteractionEnabled = false
        return textfield
    }()
    
    lazy var switchControl: UISwitch = {
        let switchControl = UISwitch()
        switchControl.isOn = true
        switchControl.onTintColor = UIColor.dcPink
        switchControl.translatesAutoresizingMaskIntoConstraints = false
        switchControl.addTarget(self, action: #selector(handleSwitchAction), for: .valueChanged)
        return switchControl
    }()

    //MARK:- Initialisers
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupSubSectionTitleTextField()
        guard let sectionType = sectionType else {return}
        textLabel?.text = sectionType.description
        if sectionType.containsSwitch == true {
            setupSwitchControl()
        }
        cellTextfield.isHidden = !sectionType.containsTextField
        if sectionType.allowsSegueToNextScreen == true {
            accessoryType = .disclosureIndicator
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Functions
    fileprivate func setupSwitchControl() {
        addSubview(switchControl)
        switchControl.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        switchControl.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
    }
    
    fileprivate func setupSubSectionTitleTextField() {
        addSubview(cellTextfield)
        cellTextfield.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        cellTextfield.rightAnchor.constraint(equalTo: rightAnchor, constant: -20).isActive = true
    }
    
    @objc func handleSwitchAction(sender: UISwitch) {
        if sender.isOn {
            print("Turned on")
        } else {
            print("Turned off")
        }
    }
}
