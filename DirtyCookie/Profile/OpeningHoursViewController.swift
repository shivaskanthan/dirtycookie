//
//  OpeningHoursViewController.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 13/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class OpeningHoursViewController: UIViewController {
    
    //MARK:- Variables and constants
    let reuseIdentifier = "openingHoursCell"
    
    // Enum containing all the available days and working hours
    enum OpeningHoursData: Int, CaseIterable, CustomStringConvertible {
        case Weekdays
        case Saturday
        case Sunday
        
        var description: String {
            switch self {
            case .Weekdays: return "Monday - Friday"
            case .Saturday: return "Saturday"
            case .Sunday: return "Sunday"
            }
        }
        
        var time: String {
            switch self {
            case .Weekdays: return "08:00 - 21:00"
            case .Saturday: return "09:00 - 18:00"
            case .Sunday: return "12:00 - 18:00"
            }
        }
    }
    
    //MARK:- IBOutlets
    let openingHoursTableView: UITableView = {
        let tv = UITableView()
        tv.rowHeight = 60
        tv.tableFooterView = UIView()
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()

    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Opening Hours"
        setupOpeningHoursTableView()
    }
    
    //MARK:- UI setup
    fileprivate func setupOpeningHoursTableView() {
        openingHoursTableView.delegate = self
        openingHoursTableView.dataSource = self
        openingHoursTableView.register(OpeningHoursTableViewCell.self, forCellReuseIdentifier: reuseIdentifier)
        view.addSubview(openingHoursTableView)
        openingHoursTableView.frame = view.frame
    }
}

//MARK:- TableView delegate and datasource
extension OpeningHoursViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return OpeningHoursData.allCases.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = openingHoursTableView.dequeueReusableCell(withIdentifier: "openingHoursCell", for: indexPath) as! OpeningHoursTableViewCell
        guard let day = OpeningHoursData(rawValue: indexPath.row) else { return OpeningHoursTableViewCell()}
        cell.textLabel?.text = day.description
        cell.timeLabel.text = day.time
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
