//
//  AuthService.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 22/09/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseFirestore

class AuthService {
    static let shared = AuthService()
    
    private init () {
        
    }
    
    // MARK:- Check if user is logged in
    func isUserLoggedIn() -> Bool {
        if Auth.auth().currentUser == nil {
            return false
        } else {
            return true
        }
    }
    
    // MARK:- Register new user to Firebase with email
    func registerUserWith(email: String, password: String, fullName: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?) -> Void){
        
        // Create new user in Firebase Auth
        Auth.auth().createUser(withEmail: email, password: password) { (firuser, error) in
            // Returns an error if user is not authenticated on Firebase Auth
            if let error = error {
                debugPrint("Failed to create a Firebase Auth user using email:", error.localizedDescription)
                // Converts an error to an error code and returns
                // let error = AuthErrorCode(rawValue: (error._code))
                userCreationComplete(false,error)
                return
            }
            
            debugPrint("Successfully authentication of user with email")
            // Check if a user was created
            guard let user = firuser?.user else { return debugPrint("There is no user returned")}
            
            //TODO: Stores user details into a dictionary
            let userData = [kUSER_STATUS: UserStatus.normal.description,
                            kUSER_FULL_NAME: fullName,
                            kUSER_EMAIL: email,
                            kUSER_CREATED_AT: FieldValue.serverTimestamp(),
                            kUSER_UPDATED_AT: FieldValue.serverTimestamp()] as [String : Any]
            
            // Save user to firestore
            DataService.shared.createDBUser(withUID: user.uid, andDetails: userData, handler: { (complete, error) in
                if let error = error {
                    // Converts an error to an error code and returns
                    // let error = AuthErrorCode(rawValue: (error._code))
                    userCreationComplete(false, error)
                    return
                }
                if complete {
                    userCreationComplete(true, nil)
                }
            })
        }
    }
    
    //MARK:- Sign in a user with email address
    func signInUserWith(email: String, password: String, signInComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        
        // Authenticate user
        Auth.auth().signIn(withEmail: email, password: password) { (firuser, error) in
            // Returns an error if user is not authenticated on Firebase Auth
            if let error = error {
                debugPrint("Failed to create a Firebase Auth user with email account: ", error.localizedDescription)
                // Converts an error to an error code and returns
                // let error = AuthErrorCode(rawValue: (error._code))
                signInComplete(false,error)
                return
            }
            debugPrint("User successfully authenticated")
            
            signInComplete(true, nil)
        }
    }
    
    //MARK:- Create new user or retrieve existing user with Firebase Database for Google and Facebook
    func signInAndRetrieveData(for provider: String, with credential: AuthCredential, handler: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().signInAndRetrieveData(with: credential) { (user, error) in
            if let error = error {
                debugPrint("Failed to create a Firebase Auth user with \(provider) account ", error.localizedDescription)
                handler(false, error)
                return
            }
            
            // Check if user exist in User database
            guard let retrivedUser = user?.user else {return print("No User")}
            DataService.shared.doesUserExist(forUID: retrivedUser.uid, userExist: { (doesExist) in
                // If user exist
                if doesExist {
                    handler(true, nil)
                } else {
                    // If user does not exist
                    debugPrint("User does not exist in User Database")
                    //TODO: Stores user details into a dictionary
                    let userData = [kUSER_STATUS: UserStatus.normal.description,
                                    kUSER_FULL_NAME: retrivedUser.displayName!,
                                    kUSER_EMAIL: retrivedUser.email!,
                                    kUSER_CREATED_AT: FieldValue.serverTimestamp(),
                                    kUSER_UPDATED_AT: FieldValue.serverTimestamp()] as [String : Any]
                    // Save user to firestore
                    DataService.shared.createDBUser(withUID: retrivedUser.uid, andDetails: userData, handler: { (complete, error) in
                        if let error = error {
                            handler(false, error)
                            return
                        }
                        if complete {
                            debugPrint("New user added using \(provider) Sign In")
                            handler(true, nil)
                        }
                    })
                }
            })
        }
    }
    
    //MARK:- Reset user password
    func resetPasswordForUserWith(email: String, emailSent: @escaping (_ status: Bool,_ error: Error?) -> ()) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let error = error {
                debugPrint("Failed to send password reset email", error.localizedDescription)
                emailSent(false, error)
                return
            }
            emailSent(true, nil)
        }
    }
    
    //MARK:- Sign out user
    func signOutUser(completion: @escaping (_ status: Bool) -> ()) {
        do {
            try Auth.auth().signOut()
            debugPrint("User successfully signed out")
            completion(true)
        } catch let error as NSError {
            debugPrint("Failed to sign out with error", error.localizedDescription)
            completion(false)
            return
        }
    }
}

//MARK:- Returns a user actionable error
extension AuthErrorCode {
    var errorMessage : String {
        switch self {
        case .userNotFound:
            return "Your account has not been created. Please sign up."
        case .userTokenExpired:
            return "Please sign in again."
        case .invalidEmail:
            return "Please check your email and sign in again."
        case .userDisabled:
            return "Your account has been disabled. Please send an email to us at welcome@thedirtycookieoc.com for further assistance"
        case .wrongPassword:
            return "Incorrect password entered. Please try again or reset password"
        case .emailAlreadyInUse:
            return "Email is already in use. Please sign in with email account."
        case .invalidUserToken:
            return "Please sign in again"
        default:
            return "An error has occured. Please drop us an email for further assistance."
        }
    }
}
