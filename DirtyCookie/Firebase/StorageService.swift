//
//  StorageService.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 07/12/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation
import FirebaseStorage

// Reference to Firebase Storage
let ST_BASE = Storage.storage().reference()

class StorageService {
    
    //MARK:- Initialise instance of Storage Service class
    static let instance = StorageService()
    
    private init () {
        
    }
    
    //MARK:- Reference to child storage in Firebase
    private var _STORAGE_BASE = ST_BASE
    private var _STORAGE_USERPROFILEIMAGE = ST_BASE.child(kSTORAGE_USER_PROFILE)
    
    var REF_BASE: StorageReference {
        return _STORAGE_BASE
    }
    var REF_USER_PROFILEIMAGE: StorageReference {
        return _STORAGE_USERPROFILEIMAGE
    }
    
    //MARK:- Upload profile image to Firebase Storage
    func uploadProfileImage(forUID uid: String, withImage image: UIImage, imageSaved: @escaping (_ status: Bool, _ error: Error?) -> Void) {
        
        let storageLocation = REF_USER_PROFILEIMAGE.child("\(uid).jpg")
        guard let profileImageData: Data = image.jpegData(compressionQuality: 0.8) else { return }
        
        storageLocation.putData(profileImageData, metadata: nil) { (metadata, error) in
            
            if let error = error {
                debugPrint("Failed to save user image to Firebase Storage", error.localizedDescription)
                imageSaved(false,error)
                return
            }
            
            guard let metadata = metadata else { return print("No metadata found",error.debugDescription)}
            print(metadata.size)
            
            storageLocation.downloadURL(completion: { (url, error) in
                if let error = error {
                    debugPrint("Failed to retrieve user image download URL", error.localizedDescription)
                    imageSaved(false,error)
                    return
                }
                guard let profileImageURL = url else { return debugPrint(error.debugDescription) }
                debugPrint(profileImageURL)
                imageSaved(true, nil)
            })
        }
    }
}

