//
//  DataService.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 22/09/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation
import FirebaseFirestore

// Reference to Firebase Firestore
fileprivate let DB_BASE = Firestore.firestore()

class DataService {
    
    //MARK:- Initialise instance of Data Service class
    static let shared = DataService()
    
    private init () {
    }
    
    //MARK:- Reference to child collection in Firestore
    private var _REF_BASE = DB_BASE
    private var _REF_USERS = DB_BASE.collection(kFIRESTORE_USERS)
    private var _REF_PRODUCTS = DB_BASE.collection(kFIRESTORE_PRODUCTS)
    
    var REF_BASE: Firestore {
        return _REF_BASE
    }
    var REF_USERS: CollectionReference {
        return _REF_USERS
    }
    var REF_PRODUCTS: CollectionReference {
        return _REF_PRODUCTS
    }
    
    //MARK:- USERS
    //MARK: Creates a new user in User Database with user details
    func createDBUser(withUID uid: String, andDetails data: [String : Any], handler: @escaping(_ status: Bool, _ error: Error?) -> ()) {
        
        REF_USERS.document(uid).setData(data) { (error) in
            if let error = error {
                debugPrint("Failed to create new user on Firebase Firestore: ", error.localizedDescription)
                handler(false, error)
                return
            }
            debugPrint("User created in Firebase Firestore")
            handler(true, nil)
        }
    }
    
    //MARK: Checks if the user already exist in Firestore
    func doesUserExist(forUID uid: String, userExist: @escaping(_ userExist: Bool) -> ()) {
        // Check if UID matches any documents in Firestore
        REF_USERS
            .document(uid)
            .getDocument { (userDocument, error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    return
                }
                if let userDocument = userDocument , userDocument.exists {
                    // User exist in Firestore
                    userExist(true)
                } else {
                    // User does exist in Firestore
                    userExist(false)
                }
        }
    }
    
    //MARK: Retrieve user details
    func retrieveUserDetailsFor(uid: String, handler: @escaping(_ user: User?, _ error: Error?) -> ()) {
        // Retrieves a snapshot of the user document using UID
        REF_USERS
            .document(uid)
            .getDocument(completion: { (userDocument, error) in
                if let error = error {
                    debugPrint(error.localizedDescription)
                    handler(nil, error)
                }
                // Parse data from Firestore into variables and stores them in a User model
                guard let userDocument = userDocument else {return debugPrint("No user document found")}
                guard let data = userDocument.data() else { return debugPrint("There is no data in this user document")}
                let fullName = data[kUSER_FULL_NAME] as? String ?? "Fullname was not retrieved"
                let email = data[kUSER_EMAIL] as? String ?? "Email was not retrieved"
                let user = User(fullName: fullName, email: email)
                handler(user, nil)
            })
    }
    
    //MARK:- PRODUCTS
    //MARK: Retrieve all products
    func retrieveAllProducts(handler: @escaping(_ productArray: [Product]?, _ error: Error?) -> ()) {
        // Initialise an array to store all products
        var products = [Product]()
        var productPrice : Price!
        // Retrieve all product documents from Firestore
        REF_PRODUCTS
            .getDocuments { (snapshot, error) in
            if let error = error {
                debugPrint(error.localizedDescription)
                handler(nil, error)
                return
            }
            // Check if snapshot returns any products
            guard let snapshot = snapshot else { return debugPrint("Products could not be fetched from Firebase")}
            for document in snapshot.documents {
                // Parse data from document
                let productID = document.documentID
                let data = document.data()
                let name = data[kPRODUCT_NAME] as? String ?? "N/A"
                let category = data[kPRODUCT_CATEGORY] as? String ?? "N/A"
                let imageURL = data[kPRODUCT_IMAGE_URL] as? String ?? ""
                // Parse data for price into a Price model
                if let price = data[kPRODUCT_PRICE] as? [String : Any] {
                    let price_single = price[kPRODUCT_PRICE_SINGLE] as? Double ?? nil
                    let price_halfDozen = price[kPRODUCT_PRICE_HALF_DOZEN] as? Double ?? nil
                    let price_eight = price[kPRODUCT_PRICE_EIGHT] as? Double ?? nil
                    let price_fullDozen = price[kPRODUCT_PRICE_FULL_DOZEN] as? Double ?? nil
                    productPrice = Price(single: price_single, halfDozen: price_halfDozen, eight: price_eight, fullDozen: price_fullDozen)
                }
                // Create Product model
                let product = Product(id: productID, name: name, category: category, imageURL: imageURL, price: productPrice)
                // Append product into an array of products
                products.append(product)
            }
            // Return an array of Products
            handler(products, nil)
        }
    }
}
