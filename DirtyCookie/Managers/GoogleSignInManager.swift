//
//  GoogleSignInManager.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 22/12/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation
import Firebase
import GoogleSignIn

protocol GoogleSignInManagerDelegate: class {
    func onGoogleSignInSuccess()
    func onGoogleSignInFail()
}

class GoogleSignInManager: NSObject, GIDSignInDelegate {
    
    weak var delegate: GoogleSignInManagerDelegate?
    
    static let shared = GoogleSignInManager()
    private override init () {
    }
    
    // Configure Google sign in
    func configure() {
        GIDSignIn.sharedInstance()?.clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    // Sign in with Google
    func signIn() {
        GIDSignIn.sharedInstance()?.signIn()
        GIDSignIn.sharedInstance()?.delegate = self
    }
    
    //
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            // Returns error if failed to sign in
            debugPrint("Failed to log in using Google: ", error.localizedDescription)
            onGoogleSignInFail()
            return
        }
        
        //TODO:- Retrieve user credentials
        print("Successfully logged into Google")
        guard  let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken, accessToken: authentication.accessToken)
        print(credential)
        AuthService.shared.signInAndRetrieveData(for: kGOOGLE, with: credential) { (complete, error) in
            if let error = error {
                debugPrint("Failed to retrieve Google user: ", error.localizedDescription)
                self.onGoogleSignInFail()
                return
            }
            if complete {
                self.onGoogleSignInSuccess()
            }
        }
    }
}

extension GoogleSignInManager: GoogleSignInManagerDelegate {
    func onGoogleSignInSuccess() {
        self.delegate?.onGoogleSignInSuccess()
    }
    
    func onGoogleSignInFail() {
        self.delegate?.onGoogleSignInFail()
    }
}
