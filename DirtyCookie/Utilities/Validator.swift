//
//  Validator.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 14.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit

struct ValidationError: LocalizedError {
    let message: String
    var errorDescription: String? { return message }
}

var errorBag: [String] = []

enum ValidationRule {
    case password
    case confirm
    case fullname
    case email
}

struct Validator {
    
    fileprivate func validate(_ condition: @autoclosure () -> Bool,
                              errorMessage messageExpression: @autoclosure () -> String) throws {
        guard condition() else {
            let message = messageExpression()
            errorBag = errorBag.filter { $0 != message }
            errorBag.append(message)
            throw ValidationError(message: message)
        }
        
        errorBag = errorBag.filter { $0 != messageExpression() }
    }
    
    public func field(rule: ValidationRule, in superView: UIView, of textField: UITextField, and matchTextField: UITextField? = nil, with constraint: NSLayoutConstraint?, _ errorLabel: UILabel) throws {
        
        errorLabel.isHidden = true
        if let constraint = constraint {
            NSLayoutConstraint.activate([constraint])
        }
        
        do {
            switch rule {
                case .password:
                    try validate(
                        textField.text!.count != 0 || textField.text != "",
                        errorMessage: "Password is required!"
                    )
                    try validate(
                        textField.text!.count >= 6,
                        errorMessage: "Password must contain at least 6 characters!"
                    )
                    try validate(
                        textField.text!.contains("0") ||
                        textField.text!.contains("1") ||
                        textField.text!.contains("2") ||
                        textField.text!.contains("3") ||
                        textField.text!.contains("4") ||
                        textField.text!.contains("5") ||
                        textField.text!.contains("6") ||
                        textField.text!.contains("7") ||
                        textField.text!.contains("8") ||
                        textField.text!.contains("9"),
                        errorMessage: "Password must contain a number!"
                    )
                case .confirm:
                    try validate(
                        textField.text!.count != 0 || textField.text != "",
                        errorMessage: "Confirm password is required!"
                    )
                    try validate(
                        textField.text == matchTextField!.text,
                        errorMessage: "Your given password does not match!"
                    )
                case .fullname:
                    try validate(
                        textField.text!.count != 0 || textField.text != "",
                        errorMessage: "Full name is required!"
                    )
                    try validate(
                        textField.text!.count >= 3,
                        errorMessage: "Fullname must contain min 3 characters!"
                    )
                case .email:
                    let pattern = "^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-+]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z‌​]{2,})$"
                    
                    try validate(
                        textField.text!.count != 0 || textField.text != "",
                        errorMessage: "Email is required!"
                    )
                    try validate(
                        NSPredicate(format: "SELF MATCHES %@", pattern).evaluate(with: textField.text), errorMessage: "Must be a valid email address!"
                    )
            }
            superView.layoutIfNeeded()
        } catch {
            if let constraint = constraint {
                NSLayoutConstraint.deactivate([constraint])
            }
            errorLabel.isHidden = false
            errorLabel.text = error.localizedDescription
            superView.layoutIfNeeded()
        }
    }
}
