//
//  ProgressHUD.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 09/12/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation
import JGProgressHUD

enum ProgressHUDResult {
    case success
    case fail
    case normal
}

struct ProgressHUD {
    
    static func showProgressHUD(type: ProgressHUDResult, view: UIView, message: String?) -> JGProgressHUD {
        let hud = JGProgressHUD(style: .dark)
        if let message = message {
         hud.textLabel.text = message
        }
        
        if type == .success {
            hud.indicatorView = JGProgressHUDSuccessIndicatorView.init()
        } else if type == .fail {
            hud.indicatorView = JGProgressHUDErrorIndicatorView.init()
            hud.tapOutsideBlock = { hud in
                hud.dismiss(animated: true)
            }
        }
        hud.show(in: view, animated: true)
        return hud
    }
}
