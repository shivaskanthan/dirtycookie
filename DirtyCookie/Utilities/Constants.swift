//
//  Constants.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 28/08/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//
//  This file stores global constants of type String

import Foundation

//MARK:- COMPANY DETAILS
public let kCOMPANY_DETAILS_NAME = "The Dirty Cookie"
public let kCOMPANY_DETAILS_EMAIL = "shivaskanthan21@gmail.com"

//MARK:- USER
public let kUSER_STATUS = "status"
public let kUSER_CREATED_AT = "createdAt"
public let kUSER_UPDATED_AT = "updatedAt"
public let kUSER_EMAIL = "email"
public let kUSER_FULL_NAME = "fullName"

//MARK:- PRODUCT-BASE
public let kPRODUCT_NAME = "name"
public let kPRODUCT_CATEGORY = "category"
public let kPRODUCT_IMAGE_URL = "imageURL"

//MARK:- PRODUCT-PRICE
public let kPRODUCT_PRICE = "price"
public let kPRODUCT_PRICE_SINGLE = "single"
public let kPRODUCT_PRICE_HALF_DOZEN = "halfDozen"
public let kPRODUCT_PRICE_EIGHT = "eight"
public let kPRODUCT_PRICE_FULL_DOZEN = "fullDozen"

//MARK:- FIRESTORE REFERENCE
public let kFIRESTORE_USERS = "USERS"
public let kFIRESTORE_PRODUCTS = "PRODUCTS"

//MARK:- STORAGE REFERENCE
public let kSTORAGE_USER_PROFILE = "USER_PROFILE_IMAGE"

//MARK:- PROVIDER
public let kGOOGLE = "Google"
public let kFACEBOOK = "Facebook"

//MARK:- NOTIFICATION KEYS
public let kSignInNotificationKey = "dc.notification.SignIn"
public let kSignUpNotificationKey = "dc.notification.SignUp"

//MARK:- URL
public let kURL_SCHEME = "https"

public let kURL_HOST_INSTAGRAM = "www.instagram.com"
public let kURL_HOST_FACEBOOK = "www.facebook.com"
public let kURL_HOST_THEDIRTYCOOKIE = "www.thedirtycookieoc.com"
public let kURL_HOST_GOOGLE_MAPS = "www.google.com.my"

public let kURL_PATH_INSTAGRAM = "/thedirtycookieoc/"
public let kURL_PATH_FACEBOOK = "/thedirtycookieoc/"
public let kURL_PATH_PRIVACYPOLICY = "/pages/privacy-policy"
public let kURL_PATH_GOOGLE_MAPS = "/maps/dir/?saddr=&daddr=\(k_LOCATION_LATITUDE),\(k_LOCATION_LONGITUDE)&directionsmode=driving"

//MARK:- EXTERNAL APP URL
public let kAPPURL_INSTAGRAM = "instagram://user?username=thedirtycookieoc"
public let kAPPURL_FACEBOOK = "fb://profile/thedirtycookieoc"
public let kAPPURL_GOOGLE_MAPS = "comgooglemaps://?saddr=&daddr=\(k_LOCATION_LATITUDE),\(k_LOCATION_LONGITUDE)&directionsmode=driving"

//MARK:- LOCATION
public let k_LOCATION_LATITUDE = 3.112747 //33.691646
public let k_LOCATION_LONGITUDE = 101.604516 //-117.858887
