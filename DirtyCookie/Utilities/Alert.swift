//
//  Alert.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 01.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//
// This file contains a reusable Alert functionality

import UIKit

struct Alert {
    
    static func showAlert(title: String, message: String?, cancelButton: Bool = false, handler: ((UIAlertAction) -> ())? = nil) -> UIAlertController {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.view.tintColor = .dcPink
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: handler))
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        if cancelButton { alertController.addAction(cancel) }
        
        return alertController
    }
    
    static func showActionSheet(title: String, message: String?, handlers: Array<UIAlertAction>) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        alertController.view.tintColor = .dcPink
        
        for i in 0..<handlers.count {
            alertController.addAction(handlers[i])
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
}
