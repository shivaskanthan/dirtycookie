//
//  CartViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 03.01.19.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {

    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationController?.navigationBar.topItem?.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeCart))
    }
    
    //MARK:- Functions
    
    //MARK: Close Cart viewController
    @objc func closeCart() {
        dismiss(animated: true, completion: nil)
    }

}
