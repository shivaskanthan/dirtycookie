//
//  ForgotPasswordViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 04.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit
import JGProgressHUD

class ForgotPasswordViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet var forgotPWView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var emailBottomConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    private var progressHUD = JGProgressHUD()
    private let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBActions
    
    //MARK: Pop to root view controller and push sign up view controller
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
        
        let name = NSNotification.Name(kSignUpNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    //MARK: Reset password
    @IBAction func resetPasswordButtonTapped(_ sender: UIButton) {
        
        do {
            try validator.field(rule: .email, in: forgotPWView, of: emailTextField, with: emailBottomConstraint, emailErrorLabel)
        } catch {
            print(error.localizedDescription)
            progressHUD.dismiss()
        }
        
        if errorBag.count == 0 {
            // Show progress HUD
            progressHUD = ProgressHUD.showProgressHUD(type: .normal, view: self.view, message: nil)
            
            // Send password reset email
            AuthService.shared.resetPasswordForUserWith(email: emailTextField.text!) { (success, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        // Dismiss progress HUD and displays reason for password reset email could not be sent
                        self.progressHUD.dismiss(animated: true)
                        // Show error message
                        self.progressHUD = ProgressHUD.showProgressHUD(type: .fail, view: self.view, message: "Failed to send email")
                        self.progressHUD.dismiss(afterDelay: 2)
                        return
                    }
                }
                if success {
                    DispatchQueue.main.async {
                        // Dismiss progress HUD and displays reason for password reset email could not be sent
                        self.progressHUD.dismiss(animated: true)
                        // Show success message
                        self.progressHUD = ProgressHUD.showProgressHUD(type: .success, view: self.view, message: "Email sent")
                        self.progressHUD.dismiss(afterDelay: 2)
                    }
                }
            }
        }
    }
}
