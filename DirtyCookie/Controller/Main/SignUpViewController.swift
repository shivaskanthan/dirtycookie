//
//  SignUpViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 21.11.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import StyleDecorator
import JGProgressHUD

class SignUpViewController: UIViewController {
    
    //MARK:- IBOutlets
    
    //MARK: View Outlets
    @IBOutlet weak var signUpView: UIView!
    
    //MARK: TextField and Label Outlets
    @IBOutlet weak var fullNameTextField: UITextField!
    @IBOutlet weak var fullNameErrorLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var confirmErrorLabel: UILabel!
    @IBOutlet weak var agreementLabel: UILabel!
    
    //MARK: Constraint Outlets
    @IBOutlet weak var fullNameBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var emailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmBottomConstraint: NSLayoutConstraint!
    
    //MARK:- Variables
    private var progressHUD = JGProgressHUD()
    private let validate = Validator()
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        agreementLabelLoad()
    }
    
    //MARK:- IBActions
    
    //MARK: Reveal password
    @IBAction func revealPasswordButtonTapped(_ sender: UIButton) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    //MARK: Sign Up with email
    @IBAction func signUpButtonTapped(_ sender: UIButton) {

        // Validate user details
        guard let email = emailTextField.text, let password = passwordTextField.text, let fullName = fullNameTextField.text else { return }
        
        do {
            try validate.field(rule: .fullname, in: signUpView, of: fullNameTextField, with: fullNameBottomConstraint, fullNameErrorLabel)
            try validate.field(rule: .email, in: signUpView, of: emailTextField, with: emailBottomConstraint, emailErrorLabel)
            try validate.field(rule: .password, in: signUpView, of: passwordTextField, with: passwordBottomConstraint, passwordErrorLabel)
            try validate.field(rule: .confirm, in: signUpView, of: confirmPasswordTextField, and: passwordTextField, with: confirmBottomConstraint, confirmErrorLabel)
        } catch {
            DispatchQueue.main.async {
                self.progressHUD.dismiss(animated: true)
            }
            print("Error during validation: ", error.localizedDescription)
        }
        // Check if there are no errors in array
        if errorBag.isEmpty {
            
            // Show progressHUD while user is being authenticated and created
            progressHUD = ProgressHUD.showProgressHUD(type: .normal, view: self.view, message: "Creating your profile...")
            
            // Authenticate user with email and password
            AuthService.shared.registerUserWith(email: email, password: password, fullName: fullName) { (isSuccess, error) in
                if isSuccess {
                    debugPrint("Successfully created a new user")
                    // Sign in user with email and password
                    AuthService.shared.signInUserWith(email: email, password: password, signInComplete: { (isSuccess, error) in
                        if isSuccess {
                            debugPrint("Successfully signed in")
                            DispatchQueue.main.async {
                                self.progressHUD.dismiss(animated: false)
                                self.progressHUD = ProgressHUD.showProgressHUD(type: .success, view: self.view, message: "Success!")
                                self.progressHUD.dismiss(animated: true)
                                // Perform Segue to Home screen
                                self.dismiss(animated: true, completion: nil)
                            }
                        } else {
                            debugPrint("Failed to sign in new user:", error!.localizedDescription)
                        }
                    })
                } else {
                    // Failed to register a new user
                    DispatchQueue.main.async {
                        self.progressHUD.dismiss(animated: true)
                        self.progressHUD = ProgressHUD.showProgressHUD(type: .fail, view: self.view, message: "We could not create your profile.")
                        return
                    }
                }
            }
        }
    }
    
    //MARK: Pop to rootViewController and push SignInViewController
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
        
        let name = NSNotification.Name(kSignInNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    //MARK:- Functions
    
    //MARK: Load agreement label with attributes
    private func agreementLabelLoad() {
        
        let defaultAttributes = Style().foregroundColor(.dcDarkBlue).attributes
        let dcPinkTextColour = Decorator(style: Style().foregroundColor(.dcPink).font(UIFont.systemFont(ofSize: 13, weight: .semibold)))
        
        let termsOfServiceText = ("Terms of Service" + dcPinkTextColour)
        let privacyPolicyText = ("Privacy Policy" + dcPinkTextColour)
        
        let decoratedAgreementText = "By signing up, I agree to The Dirty Cookie's \n" + termsOfServiceText + " and " + privacyPolicyText + "."

        agreementLabel.attributedText = NSAttributedString(decorator: decoratedAgreementText, attributes: defaultAttributes)
    }
}
