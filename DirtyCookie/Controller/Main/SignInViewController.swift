//
//  SignInViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 02.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn
import FacebookCore
import FacebookLogin
import FirebaseAuth
import JGProgressHUD

class SignInViewController: UIViewController, GIDSignInUIDelegate {

    //MARK:- IBOutlets
    
    //MARK: SignInView
    @IBOutlet weak var signInView: UIView!
    
    //MARK: TextFields
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: Constraints
    @IBOutlet weak var emailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var passwordBottomConstraint: NSLayoutConstraint!
    
    //MARK: Error Labels
    @IBOutlet weak var emailErrorLabel: UILabel!
    @IBOutlet weak var passwordErrorLabel: UILabel!
    
    //MARK:- Variables
    private var progressHUD = JGProgressHUD()
    private let validator = Validator()
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance()?.uiDelegate = self
        GoogleSignInManager.shared.delegate = self
    }
    
    //MARK:- IBActions
    
    //MARK: Pop to rootViewController and push SignUpViewController
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        navigationController?.popToRootViewController(animated: true)
        
        let name = NSNotification.Name(kSignUpNotificationKey)
        NotificationCenter.default.post(name: name, object: nil)
    }
    
    //MARK: Reveal password
    @IBAction func revealPasswordButtonTapped(_ sender: UIButton) {
        passwordTextField.isSecureTextEntry = !passwordTextField.isSecureTextEntry
    }
    
    //MARK: Sign in with facebook
    @IBAction func facebookSignInButtonTapped(_ sender: UIButton) {
        
        progressHUD = ProgressHUD.showProgressHUD(type: .normal, view: self.view, message: "Logging in with Facebook...")
        
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile, .email], viewController: self) { (result) in
            switch result {
            case .success(grantedPermissions: _, declinedPermissions: _, token: _):
                print("Successfully signed in with Facebook")
                self.firebaseSignInWithFacebook()
            case .failed(let error):
                self.progressHUD.dismiss(animated: true)
                debugPrint("Failed to sign in user with Facebook: ", error.localizedDescription)
            case .cancelled:
                self.progressHUD.dismiss(animated: true)
            }
        }
    }
    
    //MARK: Sign in with google
    @IBAction func googleSignInButtonTapped(_ sender: UIButton) {
        progressHUD = ProgressHUD.showProgressHUD(type: .normal, view: self.view, message: "Logging in with Google...")
        GoogleSignInManager.shared.signIn()
    }
    
    //MARK: Sign in with email
    @IBAction func signInWithEmailButtonTapped(_ sender: UIButton) {
        
        do {
            try validator.field(rule: .email, in: signInView, of: emailTextField, with: emailBottomConstraint, emailErrorLabel)
            try validator.field(rule: .password, in: signInView, of: passwordTextField, with: passwordBottomConstraint, passwordErrorLabel)
        } catch {
            print(error.localizedDescription)
            DispatchQueue.main.async {
                self.progressHUD.dismiss(animated: true)
            }
        }
        if errorBag.count == 0 {
            progressHUD = ProgressHUD.showProgressHUD(type: .normal, view: self.view, message: "Logging in...")
        
            AuthService.shared.signInUserWith(email: emailTextField.text!, password: passwordTextField.text!) { (isSuccess, error) in
                
                if isSuccess {
                    DispatchQueue.main.async {
                        self.progressHUD.dismiss(animated: false)
                        self.progressHUD = ProgressHUD.showProgressHUD(type: .success, view: self.view, message: "Success!")
                        self.progressHUD.dismiss(animated: true)
                        // Perform Segue to Home screen
                        self.pushHomeVC()
                    }
                } else {
                    debugPrint("Failed to sign in user", error!.localizedDescription)
                    DispatchQueue.main.async {
                        self.progressHUD.dismiss(animated: false)
                        self.progressHUD = ProgressHUD.showProgressHUD(type: .fail, view: self.view, message: "Failed to sign in")
                        self.progressHUD.dismiss(afterDelay: 2)
                        return
                    }
                }
            }
        }
    }
    
    //MARK: Push to Home view controller
    func pushHomeVC() {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: Sign in user to Firebase
    func firebaseSignInWithFacebook(){
        guard  let authenticationToken = AccessToken.current?.authenticationToken else { return }
        let credential = FacebookAuthProvider.credential(withAccessToken: authenticationToken)
        AuthService.shared.signInAndRetrieveData(for: kFACEBOOK, with: credential) { (complete, error) in
            if let error = error {
                debugPrint("Failed to sign in user", error.localizedDescription)
                DispatchQueue.main.async {
                    self.progressHUD.dismiss(animated: false)
                    self.progressHUD = ProgressHUD.showProgressHUD(type: .fail, view: self.view, message: "Failed to sign in")
                    self.progressHUD.dismiss(afterDelay: 2)
                    return
                }
            }
            DispatchQueue.main.async {
                self.progressHUD.dismiss(animated: false)
                self.progressHUD = ProgressHUD.showProgressHUD(type: .success, view: self.view, message: "Success!")
                self.progressHUD.dismiss(animated: true)
                // Segue to Home screen
                self.pushHomeVC()
            }
        }
    }
}

extension SignInViewController: GoogleSignInManagerDelegate {
    func onGoogleSignInSuccess() {
        progressHUD.dismiss(animated: false)
        progressHUD = ProgressHUD.showProgressHUD(type: .success, view: self.view, message: "Success!")
        progressHUD.dismiss(animated: true)
        // Perform Segue to Home screen
        pushHomeVC()
    }
    func onGoogleSignInFail() {
        progressHUD.dismiss(animated: false)
        progressHUD = ProgressHUD.showProgressHUD(type: .fail, view: self.view, message: "Failed to sign in")
        progressHUD.dismiss(afterDelay: 2)
    }
}
