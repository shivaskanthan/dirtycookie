//
//  WelcomeViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 02.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit
import FirebaseAuth

class WelcomeViewController: UIViewController {
    
    lazy var signUpButton: CustomButton = {
        let button = CustomButton(title: "Sign Up", backColor: .dcPink)
        return button
    }()
    
    lazy var signInButton: CustomButton = {
        let button = CustomButton(title: "Sign In", backColor: .dcPink)
        return button
    }()
    
    lazy var storeButton: CustomButton = {
        let button = CustomButton(title: "Go to Store", color: .dcPink)
        return button
    }()
    
    //MARK:- ViewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        createObserversForNavigation()
    }
    
    //MARK:- ViewWillAppear
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
        setButtons()
    }
    
    //MARK:- ViewWillDisappear
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    //MARK:- Deinitialize notification center
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK:- Functions
    
    //MARK: Create observers for navigation control
    private func createObserversForNavigation() {
        NotificationCenter.default.addObserver(self, selector: #selector(toPushViewController), name: NSNotification.Name(kSignUpNotificationKey), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(toPushViewController), name: NSNotification.Name(kSignInNotificationKey), object: nil)
    }
    
    //MARK: Push ViewControllers on the navigation stack
    @objc private func toPushViewController(_ notification: NSNotification) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        
        switch notification.name {
            
        case NSNotification.Name(kSignInNotificationKey):
            
            let vc = storyboard.instantiateViewController(withIdentifier: "SignIn") as! SignInViewController
            navigationController?.pushViewController(vc, animated: true)
            
        case NSNotification.Name(kSignUpNotificationKey):
            
            let vc = storyboard.instantiateViewController(withIdentifier: "SignUpNow") as! SignUpViewController
            navigationController?.pushViewController(vc, animated: true)
            
        default:
            print("No push view controller performed")
        }
    }
    
    // MARK:- Set Constraints
    
    fileprivate func setButtons() {
        view.addSubview(signUpButton)
        view.addSubview(signInButton)
        view.addSubview(storeButton)
        
        storeButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        storeButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        storeButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -71).isActive = true
        
        signUpButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        signUpButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        signUpButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -146).isActive = true
        
        signInButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20).isActive = true
        signInButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20).isActive = true
        signInButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -221).isActive = true
        
        storeButton.addTarget(self, action: #selector(goToStoreButtonTapped(_:)), for: .touchUpInside)
        signInButton.addTarget(self, action: #selector(signInButtonTapped(_:)), for: .touchUpInside)
        signUpButton.addTarget(self, action: #selector(signUpButtonTapped(_:)), for: .touchUpInside)
    }
    
    //MARK:- IBActions
    
    //MARK: Segue to Sign In
    @objc func signInButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "goToSignIn", sender: nil)
    }
    
    //MARK: Segue to Sign Up
    @objc func signUpButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "goToSignUp", sender: nil)
    }
    
    //MARK: Dismiss WelcomeVC
    @objc func goToStoreButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
