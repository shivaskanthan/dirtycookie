//
//  HomeTabBarController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 25.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit

class HomeTabBarController: UITabBarController {
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Present Welcome VC if user is already logged in
        if AuthService.shared.isUserLoggedIn() == false {
            DispatchQueue.main.async {
                self.goToWelcomeScreen()
            }
        }
        setupTabBar()
        setupViewControllers()
    }
    
    // Segue to Welcome Screen
    fileprivate func goToWelcomeScreen() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
        guard let welcomeVC = mainStoryboard.instantiateInitialViewController() else {
            debugPrint("Failed to retrieve WelcomeViewController")
            return
        }
        welcomeVC.modalTransitionStyle = .crossDissolve
        welcomeVC.modalPresentationStyle = .overFullScreen
        self.present(welcomeVC, animated: false, completion: nil)
    }
    
    fileprivate func setupTabBar() {
        tabBar.tintColor = UIColor.dcPink
    }
    
    fileprivate func setupViewControllers() {
        let homeNavigationController = templateNavigationController(title: "Home", image: "Tab Bar/store", rootViewController: HomeViewController())
        let favouritesViewController = templateNavigationController(title: "Favourites", image: "Tab Bar/heart", rootViewController: FavoritesViewController())
        let cartViewController = templateNavigationController(title: "Cart", image: "Tab Bar/shopping-bag", rootViewController: CartViewController())
        let ordersViewController = templateNavigationController(title: "Orders", image: "Tab Bar/receipt", rootViewController: OrdersViewController())
        viewControllers = [ homeNavigationController,
                            favouritesViewController,
                            cartViewController,
                            ordersViewController]
    }
    
    fileprivate func templateNavigationController(title: String, image: String, rootViewController: UIViewController = UIViewController()) -> UINavigationController {
        let viewController = rootViewController
        let navigationController = UINavigationController(rootViewController: viewController)
        navigationController.tabBarItem.image = UIImage(named: image)
        navigationController.tabBarItem.title = title
        return navigationController
    }
    
    //MARK:- UITabBarControllerDelegate functions
    
//    //MARK: Should Tab bar item be selected
//    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
//
//        if viewController is CartNavigationController {
//            let storyboard = UIStoryboard(name: "Home", bundle: nil)
//            if let cartVC = storyboard.instantiateViewController(withIdentifier: "cartNavVC") as? CartNavigationController {
//                cartVC.modalPresentationStyle = .fullScreen
//                self.present(cartVC, animated: true, completion: nil)
//            }
//            return false
//        }
//        return true
//    }
}
