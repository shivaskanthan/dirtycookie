//
//  SegmentView.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 29.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit

//MARK:- Protocol for ScrollMenuIndex
protocol ScrollMenuIndexDelegate: class {
    func scrollToMenuIndex(_ menuIndex: Int)
}

class HomeMenuView: UIView, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    //MARK:- Variables
    private var segmentTitles = [ProductCategory]()
    public let cellId = "segment"
    var cellWidth: CGFloat = 0
    var contentOffsetX: CGFloat = 0.0
    weak var delegate: ScrollMenuIndexDelegate?
    let horizontalBar = UIView()
    var horizontalBarLeftAnchor: NSLayoutConstraint?
    var horizontalBarWidth: NSLayoutConstraint?
    
    //MARK: collectionView for segments
    lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isScrollEnabled = true
        cv.backgroundColor = .white
        cv.showsHorizontalScrollIndicator = false
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()

    //MARK: Initialization
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Setup view
        layer.shadowColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.28)
        layer.shadowOffset = CGSize(width: 0, height: 1)
        layer.shadowRadius = 4
        layer.shadowOpacity = 1
        tintColor = .dcPink
        
        setupCollectionView()
    }
    
    //MARK:- Functions
    
    //MARK: Set titles for segment Items
    public func setTitles(_ titles: [ProductCategory]) {
        segmentTitles = titles
        collectionView.reloadData()
        
        setupIndexOfSegments()
    }
    
    //MARK: Setup collectionView
    func setupCollectionView() {
        collectionView.register(SegmentCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        
        addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: topAnchor),
            collectionView.leftAnchor.constraint(equalTo: leftAnchor),
            collectionView.rightAnchor.constraint(equalTo: rightAnchor),
            collectionView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    //MARK: Setup horizontal bar
    func setupHorizontalBar() {
        
        // Setup
        horizontalBar.backgroundColor = UIColor.dcPink
        horizontalBar.translatesAutoresizingMaskIntoConstraints = false
        collectionView.addSubview(horizontalBar)
        
        // Constraints
        horizontalBarLeftAnchor = horizontalBar.leftAnchor.constraint(equalTo: leftAnchor, constant: 5)
        horizontalBarWidth =
            horizontalBar.widthAnchor.constraint(equalToConstant: cellWidth)
        NSLayoutConstraint.activate([
            horizontalBarLeftAnchor!,
            horizontalBar.bottomAnchor.constraint(equalTo: bottomAnchor),
            horizontalBarWidth!,
            horizontalBar.heightAnchor.constraint(equalToConstant: 2),
            ])
    }
    
    //MARK: Setup index of segments
    func setupIndexOfSegments() {
        let selectedIndexPath = IndexPath(item: 0, section: 0)
        collectionView.selectItem(at: selectedIndexPath, animated: false, scrollPosition: .centeredHorizontally)
        
        let label = UILabel()
        label.text = segmentTitles[selectedIndexPath.item].rawValue
        label.textAlignment = .center
        label.sizeToFit()
        
        cellWidth = label.frame.size.width + 20
        
        setupHorizontalBar()
    }
    
    //MARK:- UICollectionViewDataSource functions
    
    //MARK: Number of items in section
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if segmentTitles.count == 0 {
            return 1
        }
        
        return segmentTitles.count 
    }
    
    //MARK: Cell for each item
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SegmentCollectionViewCell
        
        cell.categoryLabel.text = segmentTitles[indexPath.item].rawValue
        
        return cell
    }
    
    //MARK:- UICollectionViewDelegateFlowLayout functions
    
    //MARK: Cell size for collectionView
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel()
        label.text = segmentTitles[indexPath.item].rawValue
        label.textAlignment = .center
        label.sizeToFit()
        
        let width = label.frame.size.width + 20
        return CGSize(width: width, height: frame.height)
    }
    
    //MARK: Inset for section on left and right
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let totalCellWidth = (collectionView.frame.width - 10)
        
        let leftInset = (collectionView.frame.width - totalCellWidth) / 2
        let rightInset = leftInset
        
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
    //MARK: Minimum Line spacing for section
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
//        return 0
//    }
    
    //MARK:- UICollectionViewDelegate functions
    
    //MARK: When selecting Item
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        delegate?.scrollToMenuIndex(indexPath.item)
    }
    
    //MARK:- ScrollMenuIndexDelegate function
    
    //MARK: When scrollView did scroll
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contentOffsetX = scrollView.contentOffset.x
    }
}
