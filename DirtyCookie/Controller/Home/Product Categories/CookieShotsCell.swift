////
////  CookieShotsCell.swift
////  DirtyCookie
////
////  Created by Hanan Mufti on 02.01.19.
////  Copyright © 2019 Shiva Skanthan. All rights reserved.
////
//
//import UIKit
//
//class CookieShotsCell: UICollectionViewCell, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
//
//    //MARK:- Variables
//    var cellId = "productCell"
//    var cookieShots: [Product]? {
//        didSet {
//            collectionView.reloadData()
//        }
//    }
//
//    lazy var collectionView: UICollectionView = {
//        let layout = UICollectionViewFlowLayout()
//        layout.scrollDirection = .vertical
//        layout.minimumInteritemSpacing = 8
//        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
//        cv.backgroundColor = .dcLightGray
//        cv.isScrollEnabled = true
//        cv.showsVerticalScrollIndicator = true
//        cv.delegate = self
//        cv.dataSource = self
//        return cv
//    }()
//
//    //MARK:- Initialisation
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//
//        setupCollectionViewForCookieShotsCell()
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//    }
//
//    //MARK:- Functions
//
//    //MARK: Setup collectionView for Cookie shots cell
//    func setupCollectionViewForCookieShotsCell() {
//        collectionView.translatesAutoresizingMaskIntoConstraints = false
//
//        addSubview(collectionView)
//        NSLayoutConstraint.activate([
//            collectionView.topAnchor.constraint(equalTo: self.topAnchor),
//            collectionView.leftAnchor.constraint(equalTo: self.leftAnchor),
//            collectionView.rightAnchor.constraint(equalTo: self.rightAnchor),
//            collectionView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
//        ])
//        collectionView.register(UINib(nibName: "ProductCell", bundle: nil), forCellWithReuseIdentifier: "productCell")
//    }
//
//    //MARK:- UICollectionViewDataSource functions
//
//    //MARK: Number of Items in sections
//    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//
//        return cookieShots?.count ?? 0
//    }
//
//    //MARK:- Number of sections in collectionView
//    func numberOfSections(in collectionView: UICollectionView) -> Int {
//        return 1
//    }
//
//    //MARK: Cell for each Item
//    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
//        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productCell", for: indexPath) as! ProductCell
//
//        cell.productLabel.text = cookieShots?[indexPath.item].name
//
//        if let imageURL = URL(string: (cookieShots?[indexPath.item].imageURL)!) {
//            DispatchQueue.global().async {
//                let imageData: NSData = NSData(contentsOf: imageURL)!
//
//                DispatchQueue.main.async {
//                    let image = UIImage(data: imageData as Data)
//                    cell.imageView.image = image
//                }
//            }
//        }
//
//        var price = cookieShots?[indexPath.item].price.halfDozen
//        if cookieShots?[indexPath.item].name == "Pack of 8 Popular Cookie Shots" {
//            price = cookieShots?[indexPath.item].price.single
//            cell.imageView.image = UIImage(named: "Logo/logo_dirtycookie")
//        }
//        cell.priceLabel.text = "$ \(price!)0"
//        
//        return cell
//    }
//
//    //MARK:- UICollectionViewDelegateFlowLayout functions
//
//    //MARK: Cell size for collectionView
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: (frame.width - 16), height: 130)
//    }
//
//    //MARK: Cell insets for sections
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
//    }
//}
