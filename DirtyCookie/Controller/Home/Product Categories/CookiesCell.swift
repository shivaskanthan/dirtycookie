//
//  CookiesCell.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 02.01.19.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class CookiesCell: UICollectionViewCell {
    
    var cookies: [Product]! {
        didSet {
            print("Passed value is: \(cookies)")
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
