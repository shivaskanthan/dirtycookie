//
//  HomeMenuBar.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 21/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class HomeMenuBar: UIView, UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {
    
    //MARK:- Variables and constants
    let cellID = "cellID"
    var cellWidth: CGFloat = 0
    var homeVC = HomeViewController()
    // Changes the width of the horizontal bar based on the length of text length of the selected product category
    var horizontalBarWidth: NSLayoutConstraint?
    // Offsets the x-coordinate of the horizontal bar to the selected product category
    var contentOffsetX: CGFloat = 0.0
    // Once an offset is determined, the left constraint of the horizontal bar is updated
    var horizontalBarLeftAnchorConstraint: NSLayoutConstraint?
    
    //MARK:- IBOutlets
    lazy var productCategoryCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .white
        cv.dataSource = self
        cv.delegate = self
        cv.register(HomeMenuBarCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        return cv
    }()
    
    let horizontalBar: UIView = {
        let view = UIView()
        view.backgroundColor = .dcPink
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    //MARK:- Initialisers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setup UI
    fileprivate func setupUI() {
        setupProductCategoryCollectionView()
        setupHorizontalBar()
    }
    
    fileprivate func setupProductCategoryCollectionView() {
        addSubview(productCategoryCollectionView)
        productCategoryCollectionView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
        
        let initialIndexPath = IndexPath(item: 0, section: 0)
        productCategoryCollectionView.selectItem(at: initialIndexPath, animated: true, scrollPosition: [])
    }
    
    fileprivate func setupHorizontalBar() {
        addSubview(horizontalBar)
        horizontalBar.heightAnchor.constraint(equalToConstant: 2).isActive = true
        horizontalBar.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
        // Calculate width of product title cell and use that width as the initial width of the horizontal menu bar
        let categoryName = ProductCategory(rawValue: 0)?.description
        cellWidth = getWidth(forCategory: categoryName!)
        horizontalBarWidth = horizontalBar.widthAnchor.constraint(equalToConstant: cellWidth)
        horizontalBarWidth?.isActive = true
        
        // Set the initial left anchor for the horizontal menu bar
        horizontalBarLeftAnchorConstraint = horizontalBar.leftAnchor.constraint(equalTo: self.leftAnchor)
        horizontalBarLeftAnchorConstraint?.isActive = true
    }
    
    // Returns the width for a given product title. This will be used to set the width of the horizontalMenuBar
    fileprivate func getWidth(forCategory category: String) -> CGFloat {
        let label = UILabel()
        label.text = category
        label.textAlignment = .center
        label.sizeToFit()
        let width = label.frame.size.width + 20
        return width
    }
    
    //MARK: CollectionView delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductCategory.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let productCategoryName = ProductCategory(rawValue: indexPath.item)?.description
        let width = getWidth(forCategory: productCategoryName!)
        return CGSize(width: width, height: frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! HomeMenuBarCollectionViewCell
        let productCategoryName = ProductCategory(rawValue: indexPath.item)?.description
        cell.productTitleLabel.text = productCategoryName
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // Scrolls to selected category
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        // Scrolls the HomeCollectionView to the selected producted category in MenuBar
        homeVC.scrollProductsCollectionViewToCategoryWith(menuBarIndex: indexPath.item)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        contentOffsetX = scrollView.contentOffset.x
    }
}
