//
//  HomeCollectionViewCell.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 21/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    let whiteBackgroundView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 14
        view.layer.masksToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let productImageView: UIImageView = {
        let iv = UIImageView()
        iv.image = UIImage(named: "Chocolate Chip")
        iv.backgroundColor = .red
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    let productNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Chocolate Chip"
        label.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        label.textAlignment = .left
        return label
    }()
    
    let productPriceLabel: UILabel = {
        let label = UILabel()
        label.text = "$ 5.50"
        label.font = UIFont.systemFont(ofSize: 18, weight: .regular)
        label.textColor = UIColor.gray
        label.textAlignment = .left
        return label
    }()
    
    //MARK:- Initialisers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Setup UI
    fileprivate func setupUI() {
        backgroundColor = UIColor.collectionViewCellBackgroundColour
        setupBackgroundView()
        setupProductImageView()
        setupProductNameLabel()
        setupProductPriceLabel()
    }
    
    fileprivate func setupBackgroundView() {
        addSubview(whiteBackgroundView)
        whiteBackgroundView.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 8, paddingLeft: 8, paddingBottom: 8, paddingRight: 8, width: 0, height: 0)
    }
    
    fileprivate func setupProductImageView() {
        whiteBackgroundView.addSubview(productImageView)
        productImageView.anchor(top: whiteBackgroundView.topAnchor, left: whiteBackgroundView.leftAnchor, bottom: whiteBackgroundView.bottomAnchor, right: nil, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 130, height: 0)
    }
    
    fileprivate func setupProductNameLabel() {
        whiteBackgroundView.addSubview(productNameLabel)
        productNameLabel.anchor(top: whiteBackgroundView.topAnchor, left: productImageView.rightAnchor, bottom: nil, right: whiteBackgroundView.rightAnchor, paddingTop: 20, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    fileprivate func setupProductPriceLabel() {
        whiteBackgroundView.addSubview(productPriceLabel)
        productPriceLabel.anchor(top: productNameLabel.bottomAnchor, left: productNameLabel.leftAnchor, bottom: nil, right: whiteBackgroundView.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    func addDetails(product: Product) {
        productNameLabel.text = product.name
        productPriceLabel.text = "$ \(product.price.halfDozen ?? 0)0"
    }
}
