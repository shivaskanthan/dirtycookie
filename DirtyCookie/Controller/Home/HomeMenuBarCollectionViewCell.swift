//
//  HomeMenuBarCollectionViewCell.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 21/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class HomeMenuBarCollectionViewCell: UICollectionViewCell {
    
    let productTitleLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        label.textColor = .gray
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    //MARK:- Initialisers
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func setupUI() {
        backgroundColor = .white
        setupProductTitleLabel()
    }
    
    fileprivate func setupProductTitleLabel() {
        addSubview(productTitleLabel)
        productTitleLabel.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    //MARK:- CollectionViewCell Delegates
    override var isSelected: Bool {
        didSet {
            if isSelected {
                productTitleLabel.textColor = UIColor.dcPink
                productTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
            } else {
                productTitleLabel.textColor = .gray
                productTitleLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
            }
        }
    }
}
