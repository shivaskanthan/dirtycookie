//
//  StoreViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 23.12.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    // MARK:- Variables
    let cellID = "cellID"
//    private var categories: [ProductCategory] = [.cookieShot, .cookieButter, .cookies]
    public var cookieShots: [Product]!
    public var cookies: [Product]!
    public var cookieButters: [Product]!
    
    //MARK:- IBOutlets
    lazy var menuBar: HomeMenuBar = {
        let mb = HomeMenuBar()
        mb.homeVC = self
        return mb
    }()
    
    lazy var productsCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        cv.backgroundColor = UIColor.collectionViewCellBackgroundColour
        cv.dataSource = self
        cv.delegate = self
        cv.register(UICollectionViewCell.self, forCellWithReuseIdentifier: cellID)
//        collectionView.register(HomeCollectionViewCell.self, forCellWithReuseIdentifier: cellID)
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv
    }()
    
    // MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        allocateProductsForCells()
    }
    
    //MARK:- Setup UI
    fileprivate func setupUI() {
        setupNavigationBar()
        setupMenuBar()
        setupCollectionView()
    }
    
    fileprivate func setupNavigationBar() {
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.tintColor = UIColor.dcPink
        navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.dcPink]
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.dcPink]
        self.navigationItem.title = kCOMPANY_DETAILS_NAME
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "Navigation Bar/settings"), style: .plain, target: self, action: #selector(goToProfile))
    }
    
    fileprivate func setupMenuBar() {
        view.addSubview(menuBar)
        menuBar.anchor(top: view.safeAreaLayoutGuide.topAnchor, left: view.leftAnchor, bottom: nil, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 45)
    }
    
    fileprivate func setupCollectionView() {
        view.addSubview(productsCollectionView)
        productsCollectionView.anchor(top: menuBar.bottomAnchor, left: view.leftAnchor, bottom: view.bottomAnchor, right: view.rightAnchor, paddingTop: 0, paddingLeft: 0, paddingBottom: 0, paddingRight: 0, width: 0, height: 0)
    }
    
    //MARK:- IBActions
    // Segue to User Profile
    @objc fileprivate func goToProfile() {
        let profileVC = ProfileViewController()
        navigationController?.pushViewController(profileVC, animated: true)
    }
    
    //MARK:- Functions
    // Retrieve product data and fill into arrays for each product category
    fileprivate func allocateProductsForCells() {
        DataService.shared.retrieveAllProducts { [unowned self] (products, error) in
            guard let products = products else { return debugPrint("Could not retrieve products from Firestore") }
            
            self.cookieShots = retrieveProducts(for: .cookieShot, from: products)
            self.cookies = retrieveProducts(for: .cookies, from: products)
            self.cookieButters = retrieveProducts(for: .cookieButter, from: products)
            
            self.productsCollectionView.reloadData()
        }
    }
    
    // Scrolls the productsCollectionView to the category selected on the MenuBar
    func scrollProductsCollectionViewToCategoryWith(menuBarIndex: Int) {
        let indexPath = IndexPath(item: menuBarIndex, section: 0)
        productsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    //MARK:- productsCollectionView delegates
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ProductCategory.allCases.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath)
        let colorsArray : [UIColor] = [.orange, .green, .yellow]
        cell.backgroundColor = colorsArray[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        // Determine the indexPath
        let rounded = round((scrollView.contentOffset.x / scrollView.contentSize.width) * CGFloat(ProductCategory.allCases.count))
        let indexPath = IndexPath(item: Int(rounded), section: 0)
        
        // Use the indexPath to retrieve the current cell
        let cell = menuBar.productCategoryCollectionView.dequeueReusableCell(withReuseIdentifier: menuBar.cellID, for: indexPath) as! HomeMenuBarCollectionViewCell
        
        // Set a new left anchor of the horizontalBar based on the selected product category
        let x = cell.frame.origin.x
        menuBar.horizontalBarLeftAnchorConstraint?.constant = x - menuBar.contentOffsetX
        
        // Set a new horizontalBar width based on text length of the selected product category
        menuBar.cellWidth = cell.frame.width
        menuBar.horizontalBar.removeConstraint(menuBar.horizontalBarWidth!)
        menuBar.horizontalBarWidth = menuBar.horizontalBar.widthAnchor.constraint(equalToConstant: menuBar.cellWidth)
        menuBar.horizontalBarWidth?.isActive = true
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        // Determine the indexPath when the dragging ends
        let index = Int(targetContentOffset.pointee.x / view.frame.width)
        let indexPath = IndexPath(item: index, section: 0)
        
        // Use the indexPath to select the product category in the menuBar
        menuBar.productCategoryCollectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    //    override func numberOfSections(in collectionView: UICollectionView) -> Int {
    //        return 1
    //    }
    //
    //    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return cookieShots?.count ?? 0
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: view.frame.width, height: 138)
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! HomeCollectionViewCell
    //        if cookieShots.count >  0 {
    //            let product = cookieShots[indexPath.row]
    //            cell.addDetails(product: product)
    //        }
    //        return cell
    //    }
    
    //MARK:- UICollectionViewDataSource functions
    
    //    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    //        return categories.count
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    //
    //        let identifier: String
    //
    //        switch indexPath.item {
    //            case 1:
    //                identifier = cookieButtersCellId
    //            case 2:
    //                identifier = cookieCellId
    //            default:
    //                identifier = cookieShotsCellId
    //        }
    //
    //        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath)
    //
    //        switch indexPath.item {
    //            case 1:
    //                (cell as! CookieButtersCell).cookieButters = cookieButters
    //            case 2:
    //                (cell as! CookiesCell).cookies = cookies
    //            default:
    //                (cell as! CookieShotsCell).cookieShots = cookieShots
    //        }
    //
    //        return cell
    //    }
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //        return CGSize(width: view.frame.width, height: collectionView.frame.height)
    //    }
    //
    //
    //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
    //        return 0
    //    }
    //
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        let rounded = round((scrollView.contentOffset.x / scrollView.contentSize.width) * CGFloat(categories.count))
    //        let indexPath = IndexPath(item: Int(rounded), section: 0)
    //        let cell = segmentView.collectionView.dequeueReusableCell(withReuseIdentifier: segmentView.cellId, for: indexPath) as! SegmentCollectionViewCell
    //        let width = cell.frame.size.width
    //
    //        segmentView.horizontalBar.removeConstraint(segmentView.horizontalBarWidth!)
    //        segmentView.horizontalBarWidth = segmentView.horizontalBar.widthAnchor.constraint(equalToConstant: width)
    //        segmentView.horizontalBarWidth?.isActive = true
    //
    //        let x = cell.frame.origin.x
    //        segmentView.horizontalBarLeftAnchor?.constant = x - segmentView.contentOffsetX
    //    }
    //
    //    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
    //
    //        let index = Int(targetContentOffset.pointee.x / view.frame.width)
    //        let indexPath = IndexPath(item: index, section: 0)
    //
    //        segmentView.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    //    }
    //
    //    func scrollToMenuIndex(_ menuIndex: Int) {
    //        let indexPath = IndexPath(item: menuIndex, section: 0)
    //        productsCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    //    }
}
