//
//  FavoritesViewController.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 03.01.19.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class FavoritesViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
    }
}
