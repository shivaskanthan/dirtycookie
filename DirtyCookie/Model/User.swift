//
//  User.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 15/09/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//
//

import Foundation

class User {
    
    var fullName: String
    var email: String
    
    init(fullName: String, email: String) {
        
        self.fullName = fullName
        self.email = email
    }
}

enum UserStatus: CustomStringConvertible {
    case normal
    case inactive
    
    var description: String {
        switch self {
        case .normal: return "normal"
        case .inactive: return "inactive"
        }
    }
}
