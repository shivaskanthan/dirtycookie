//
//  Product.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 27/12/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation

enum ProductCategory : Int, CaseIterable, CustomStringConvertible {
    case cookieShot
    case cookies
    case cookieButter
    
    var description: String {
        switch self {
        case .cookieShot: return "Cookie Shot"
        case . cookies: return "Cookies"
        case . cookieButter: return "Cookie Butter"
        }
    }
}

class Product {
    
    var id: String
    var name: String
    var category: String
    var imageURL: String?
    var price: Price
    
    init(id: String, name: String, category: String, imageURL: String, price: Price) {
        self.id = id
        self.name = name
        self.category = category
        self.imageURL = imageURL
        self.price = price
    }
}

func retrieveProducts(for selectedCategory: ProductCategory, from unfilteredProductArray: [Product]) -> [Product] {
    return unfilteredProductArray.filter({ (product) -> Bool in
        product.category == selectedCategory.description
    })
}
