//
//  Price.swift
//  DirtyCookie
//
//  Created by Shiva Skanthan on 27/12/2018.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//

import Foundation

class Price {
    var single: Double?
    var halfDozen: Double?
    var eight: Double?
    var fullDozen: Double?
    
    init (single: Double?, halfDozen: Double?, eight: Double?, fullDozen: Double?) {
        self.single = single
        self.halfDozen = halfDozen
        self.eight = eight
        self.fullDozen = fullDozen
    }
}
