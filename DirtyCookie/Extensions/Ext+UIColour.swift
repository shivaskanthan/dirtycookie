//
//  UIColor+DCColors.swift
//  DirtyCookie
//
//  Created by Hanan Mufti on 30.11.18.
//  Copyright © 2018 Shiva Skanthan. All rights reserved.
//
// This file contains uniqe colours for DirtyCookie

import UIKit

extension UIColor {
    static var dcPink: UIColor { return #colorLiteral(red: 1, green: 0.1764705882, blue: 0.3333333333, alpha: 1) }
    static var dcDarkBlue: UIColor { return #colorLiteral(red: 0.2156862745, green: 0.2274509804, blue: 0.2392156863, alpha: 1) }
    static var dcSteelGrey: UIColor { return #colorLiteral(red: 0.4431372549, green: 0.462745098, blue: 0.4784313725, alpha: 1) }
    static var dcLightGray: UIColor { return #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1) }
    static var collectionViewCellBackgroundColour: UIColor { return #colorLiteral(red: 0.9647058824, green: 0.9647058824, blue: 0.9647058824, alpha: 1)}
}
