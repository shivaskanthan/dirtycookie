//
//  CustomButton.swift
//  DirtyCookie
//
//  Created by Meitar Basson on 06/04/2019.
//  Copyright © 2019 Shiva Skanthan. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layer.cornerRadius = 9
        layer.masksToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    convenience init(title: String, backColor: UIColor) {
        self.init(frame: .zero)
        setTitle(title, for: .normal)
        backgroundColor = backColor
    }
    
    convenience init(title: String, color: UIColor) {
        self.init(frame: .zero)
        setTitle(title, for: .normal)
        setTitleColor(color, for: .normal)
        layer.borderColor = color.cgColor
        layer.borderWidth = 1
        
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
    }
    
}
